#!/bin/bash

source "$(dirname ${0})/env/bin/activate"

exec "$(dirname ${0})/manage.py" ${@}
