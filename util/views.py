from django.conf import settings
from django.http import HttpResponse
from django.core.management import call_command
from django.views.decorators.csrf import csrf_exempt

import subprocess
import os


@csrf_exempt
def pull(request):
	response = HttpResponse(content_type="text/plain")

	print("Pulling from git repository:", file=response)
	git = subprocess.Popen(
		["/usr/bin/git", "pull"],
		stdout=subprocess.PIPE,
		stderr=subprocess.STDOUT,
		cwd=settings.BASE_DIR
	)
	try:
		out, _err = git.communicate(timeout=15)
	except subprocess.TimeoutExpired:
		git.kill()
		out, _err = git.communicate()
	print(out.decode(), file=response)

	print("Installing new deps:", file=response)

	try:
		import uwsgi
		pip = os.path.join(uwsgi.opt['home'].decode(), "bin/pip")
	except ImportError:
		# crutch, won't work if env is not in BASE_DIR/env
		pip = os.path.join(settings.BASE_DIR, "env/bin/pip")

	pip = subprocess.Popen(
		[pip, "install", "-r", "requirements.txt", "--no-color"],
		stdout=subprocess.PIPE,
		stderr=subprocess.STDOUT,
		cwd=settings.BASE_DIR,
	)
	try:
		out, _err = pip.communicate(timeout=30)
	except subprocess.TimeoutExpired:
		pip.kill()
		out, _err = pip.communicate()
	print(out.decode(), file=response)

	print("Migrating database:", file=response)
	call_command('migrate', '--no-color', '--no-input', stdout=response)
	print('', file=response)

	print("Collecting static files:", file=response)
	call_command('collectstatic', '--no-color', '--no-input', stdout=response)
	print('', file=response)

	print("Reloading uWSGI app:", file=response)
	try:
		import uwsgi
	except ImportError:
		print("Not running under uWSGI", file=response)
	else:
		uwsgi.reload()
		print("Reloaded uWSGI", file=response)

	return response
