from django.contrib import admin
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.http import url_has_allowed_host_and_scheme


class NoAuthAdminSite(admin.AdminSite):
	def login(self, request):
		user = authenticate(request)
		login(request, user)
		redirect_to = request.GET.get('next', '')
		url_allowed = url_has_allowed_host_and_scheme(
			url=redirect_to, allowed_hosts=request.get_host(), require_https=False
		)
		if url_allowed:
			return HttpResponseRedirect(redirect_to)
		else:
			return HttpResponseRedirect(reverse('admin:index', current_app=self.name))
