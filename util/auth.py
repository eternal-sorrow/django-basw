from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User


class DummyBackend(BaseBackend):
	def authenticate(self, request):
		user = User.objects.first()
		if user is None:
			user = User.objects.create(username='admin', is_staff=True, is_superuser=True)
		return user

	def get_user(self, user_id):
		return User.objects.first()
